# Vincent Pinon <vpinon@kde.org>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-09 03:41+0200\n"
"PO-Revision-Date: 2019-03-11 23:54+0100\n"
"Last-Translator: Vincent Pinon <vpinon@kde.org>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 2.0\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../reference_manual/dockers/advanced_color_selector.rst:1
msgid "Overview of the advanced color selector docker."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:11
#: ../../reference_manual/dockers/advanced_color_selector.rst:16
msgid "Advanced Color Selector"
msgstr "Sélecteur de couleurs avancé"

#: ../../reference_manual/dockers/advanced_color_selector.rst:11
#: ../../reference_manual/dockers/advanced_color_selector.rst:24
msgid "Color Selector"
msgstr "Sélecteur de couleurs"

#: ../../reference_manual/dockers/advanced_color_selector.rst:11
#, fuzzy
#| msgid "Color Space"
msgid "Color"
msgstr "Espace colorimétrique"

#: ../../reference_manual/dockers/advanced_color_selector.rst:20
msgid ".. image:: images/dockers/Advancecolorselector.jpg"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:21
msgid ""
"As compared to other color selectors in Krita, Advanced color selector "
"provides more control and options to the user. To open Advanced color "
"selector choose :menuselection:`Settings --> Dockers --> Advanced Color "
"Selector`. You can configure this docker by clicking on the little wrench "
"icon on the top left corner. Clicking on the wrench will open a popup window "
"with following tabs and options:"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:26
msgid "Here you configure the main selector."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:28
#, fuzzy
#| msgid "Color Selector"
msgid "Show Color Selector"
msgstr "Sélecteur de couleurs"

#: ../../reference_manual/dockers/advanced_color_selector.rst:31
msgid ""
"This allows you to configure whether to show or hide the main color selector."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:34
msgid "Type and Shape"
msgstr "Type et forme"

#: ../../reference_manual/dockers/advanced_color_selector.rst:37
msgid ".. image:: images/dockers/Krita_Color_Selector_Types.png"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:38
msgid ""
"Here you can pick the hsx model you'll be using. There's a small blurb "
"explaining the characteristic of each model, but let's go into detail:"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:41
msgid "HSV"
msgstr "TSV"

#: ../../reference_manual/dockers/advanced_color_selector.rst:42
msgid ""
"Stands for Hue, Saturation, Value. Saturation determines the difference "
"between white, gray, black and the most colorful color. Value in turn "
"measures either the difference between black and white, or the difference "
"between black and the most colorful color."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:43
msgid "HSL"
msgstr "TSL"

#: ../../reference_manual/dockers/advanced_color_selector.rst:44
msgid ""
"Stands for Hue, Saturation, Lightness. All saturated colors are equal to 50% "
"lightness. Saturation allows for shifting between gray and color."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:45
msgid "HSI"
msgstr "TSI"

#: ../../reference_manual/dockers/advanced_color_selector.rst:46
msgid ""
"This stands for Hue, Saturation and Intensity. Unlike HSL, this one "
"determine the intensity as the sum of total rgb components. Yellow (1,1,0) "
"has higher intensity than blue (0,0,1) but is the same intensity as cyan "
"(0,1,1)."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:48
msgid "HSY'"
msgstr "HSY'"

#: ../../reference_manual/dockers/advanced_color_selector.rst:48
msgid ""
"Stands for Hue, Saturation, Luma, with Luma being an RGB approximation of "
"true luminosity. (Luminosity being the measurement of relative lightness). "
"HSY' uses the Luma Coefficients, like `Rec 709 <http://en.wikipedia.org/wiki/"
"Rec._709>`_, to calculate the Luma. Due to this, HSY' can be the most "
"intuitive selector to work with, or the most confusing."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:50
msgid ""
"Then, under shape, you can select one of the shapes available within that "
"color model."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:54
msgid ""
"Triangle is in all color models because to a certain extent, it is a "
"wildcard shape: All color models look the same in an equilateral triangle "
"selector."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:57
msgid "Luma Coefficients"
msgstr "Coefficients de luminance"

#: ../../reference_manual/dockers/advanced_color_selector.rst:59
msgid ""
"This allows you to edit the Luma coefficients for the HSY model selectors to "
"your leisure. Want to use `Rec 601 <http://en.wikipedia.org/wiki/Rec._601>`_ "
"instead of Rec 709? These boxes allow you to do that!"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:61
msgid "By default, the Luma coefficients should add up to 1 at maximum."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:64
msgid "Gamma"
msgstr "Gamma"

#: ../../reference_manual/dockers/advanced_color_selector.rst:64
msgid ""
"The HSY selector is linearised, this setting allows you to choose how much "
"gamma is applied to the Luminosity for the gui element. 1.0 is fully linear, "
"2.2 is the default."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:67
msgid "Color Space"
msgstr "Espace colorimétrique"

#: ../../reference_manual/dockers/advanced_color_selector.rst:69
msgid ""
"This allows you to set the overall color space for the Advanced Color "
"Selector."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:72
msgid ""
"You can pick only sRGB colors in advanced color selector regardless of the "
"color space of advanced color selector. This is a bug."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:75
#, fuzzy
#| msgid "Behaviour"
msgid "Behavior"
msgstr "Comportement"

#: ../../reference_manual/dockers/advanced_color_selector.rst:78
msgid "When docker resizes"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:80
msgid "This determines the behavior of the widget as it becomes smaller."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:82
msgid "Change to Horizontal"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:83
msgid ""
"This'll arrange the shade selector horizontal to the main selector. Only "
"works with the MyPaint shade selector"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:84
msgid "Hide Shade Selector"
msgstr "Masquer le sélecteur d'ombre"

#: ../../reference_manual/dockers/advanced_color_selector.rst:85
msgid "This hides the shade selector."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:87
msgid "Do nothing"
msgstr "Ne rien faire"

#: ../../reference_manual/dockers/advanced_color_selector.rst:87
msgid "Does nothing, just resizes."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:90
msgid "Zoom selector UI"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:92
msgid ""
"If your have set the docker size considerably smaller to save space, this "
"option might be helpful to you. This allows you to set whether or not the "
"selector will give a zoomed view of the selector in a size specified by you, "
"you have these options for the zoom selector:"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:94
msgid "when pressing middle mouse button"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:95
msgid "on mouse over"
msgstr "au passage de la souris"

#: ../../reference_manual/dockers/advanced_color_selector.rst:96
msgid "never"
msgstr "jamais"

#: ../../reference_manual/dockers/advanced_color_selector.rst:98
msgid ""
"The size given here, is also the size of the Main Color Selector and the "
"MyPaint Shade Selector when they are called with :kbd:`Shift + I` and :kbd:"
"`Shift + M`, respectively."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:101
msgid "Hide Pop-up on click"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:101
msgid ""
"This allows you to let the pop-up selectors called with the above hotkeys to "
"disappear upon clicking them instead of having to leave the pop-up boundary. "
"This is useful for faster working."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:104
msgid "Shade selector"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:106
msgid ""
"Shade selector options. The shade selectors are useful to decide upon new "
"shades of color."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:110
msgid "Update Selector"
msgstr "Sélection des mises à jour"

#: ../../reference_manual/dockers/advanced_color_selector.rst:112
msgid "This allows you to determine when the shade selector updates."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:115
msgid "MyPaint Shade Selector"
msgstr "Outil de sélection d'ombre MyPaint"

#: ../../reference_manual/dockers/advanced_color_selector.rst:117
msgid ""
"Ported from MyPaint, and extended with all color models. Default hotkey is :"
"kbd:`Shift+ M`"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:121
msgid "Simple Shade Selector"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:123
msgid "This allows you to configure the simple shade selector in detail."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:126
msgid "Color Patches"
msgstr "Correctifs de couleur"

#: ../../reference_manual/dockers/advanced_color_selector.rst:128
msgid "This sets the options of the color patches."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:130
msgid ""
"Both Color History and Colors From the Image have similar options which will "
"be explained below."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:132
msgid "Show"
msgstr "Afficher"

#: ../../reference_manual/dockers/advanced_color_selector.rst:133
msgid ""
"This is a radio button to show or hide the section. It also determines "
"whether or not the colors are visible with the advanced color selector "
"docker."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:134
msgid "Size"
msgstr "Taille"

#: ../../reference_manual/dockers/advanced_color_selector.rst:135
msgid "The size of the color boxes can be set here."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:136
msgid "Patch Count"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:137
msgid "The number of patches to display."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:138
msgid "Direction"
msgstr "Direction"

#: ../../reference_manual/dockers/advanced_color_selector.rst:139
msgid "The direction of the patches, Horizontal or Vertical."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:140
msgid "Allow Scrolling"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:141
msgid ""
"Whether to allow scrolling in the section or not when there are too many "
"patches."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:142
msgid "Number of Columns/Rows"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:143
msgid "The number of Columns or Rows to show in the section."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:145
msgid "Update After Every Stroke"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:145
msgid ""
"This is only available for Colors From the Image and tells the docker "
"whether to update the section after every stroke or not, as after each "
"stroke the colors will change in the image."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:148
msgid "History patches"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:150
msgid ""
"The history patches remember which colors you've drawn on canvas with. They "
"can be quickly called with :kbd:`H`"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:153
msgid "Common Patches"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:155
msgid ""
"The common patches are generated from the image, and are the most common "
"color in the image. The hotkey for them on canvas is :kbd:`U`."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:158
msgid "Gamut masking"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:164
msgid ""
"Gamut masking is available only when the selector shape is set to wheel."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:166
msgid ""
"You can select and manage your gamut masks in the :ref:`gamut_mask_docker`."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:168
msgid ""
"In the gamut masking toolbar at the top of the selector you can toggle the "
"selected mask off and on (left button). You can also rotate the mask with "
"the rotation slider (right)."
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:173
msgid "External Info"
msgstr ""

#: ../../reference_manual/dockers/advanced_color_selector.rst:175
msgid ""
"`HSI and HSY for Krita’s advanced color selector. <http://wolthera.info/?"
"p=726>`_"
msgstr ""

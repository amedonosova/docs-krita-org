msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-24 09:15+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: Krita Mic guilabel\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../reference_manual/layers_and_masks/filter_layers.rst:1
msgid "How to use filter layers in Krita."
msgstr "Como usar as camadas de filtragem no Krita."

#: ../../reference_manual/layers_and_masks/filter_layers.rst:14
msgid "Layers"
msgstr "Camadas"

#: ../../reference_manual/layers_and_masks/filter_layers.rst:14
msgid "Filters"
msgstr "Filtros"

#: ../../reference_manual/layers_and_masks/filter_layers.rst:19
msgid "Filter Layer"
msgstr "Camada de Filtro"

#: ../../reference_manual/layers_and_masks/filter_layers.rst:22
msgid ""
"Filter layers show whatever layers are underneath them, but with a filter "
"such as Layer Styles, Blur, Levels, Brightness / Contrast. For example, if "
"you add a **Filter Layer**, and choose the Blur filter, you will see every "
"layer under your filter layer blurred."
msgstr ""
"As camadas de filtragem mostram todos os tipos de camadas que se encontram "
"abaixo delas, mas como um filtro associado, como os Estilos das Camadas, o "
"Borrão, os Níveis ou o Brilho / Contraste. Por exemplo, se adicionar uma "
"**Camada de Filtragem** e escolher o filtro Borrão, irá ver todas as camadas "
"abaixo da sua camada de filtragem borradas."

#: ../../reference_manual/layers_and_masks/filter_layers.rst:24
msgid ""
"Unlike applying a filter directly on to a section of a Paint Layer, Filter "
"Layers do not actually alter the original image in the Paint Layers below "
"them. Once again, non-destructive editing! You can tweak the filter at any "
"time, and the changes can always be altered or removed."
msgstr ""
"Ao contrário de aplicar um filtro directamente sobre uma secção de uma "
"Camada de Pintura, as Camadas de Filtragem não alteram de facto a imagem "
"original na Camada de Pintura abaixo delas. Mais uma vez, é uma edição não-"
"destrutiva! Poderá afinal o filtro em qualquer altura, pelo que as "
"alterações podem sempre ser alteradas ou removidas."

#: ../../reference_manual/layers_and_masks/filter_layers.rst:26
msgid ""
"Unlike Filter Masks though, Filter Layers apply to the entire canvas for the "
"layers beneath. If you wish to apply a filter layer to only *some* layers, "
"then you can utilize the Group Layer feature and add those layers into a "
"group with the filter layer on top of the stack."
msgstr ""
"Ao contrário das Máscaras de Filtragem, por outro lado, as Camadas de "
"Filtragem aplicam-se a toda a área de desenho para as camadas abaixo delas. "
"Se desejar aplicar uma camada de filtragem a apenas *algumas* camadas, então "
"poderá usar a funcionalidade da Camada de Grupo e adicionar essas camadas a "
"um grupo com a camada de filtragem no topo da pilha."

#: ../../reference_manual/layers_and_masks/filter_layers.rst:28
msgid ""
"You can edit the settings for a filter layer, by double clicking on it in "
"the Layers docker."
msgstr ""
"Poderá editar a configuração de uma camada de filtragem, fazendo para tal "
"duplo-click sobre ela na área de Camadas."

#: ../../reference_manual/layers_and_masks/filter_layers.rst:31
msgid ""
"Only Krita native filters (the ones in the :guilabel:`Filters` menu) can be "
"used with Filter Layers. Filter Layers are not supported using the "
"externally integrated G'Mic filters."
msgstr ""
"Só os filtros nativos do Krita (os do menu :guilabel:`Filtros`) poderão ser "
"usados com as Camadas de Filtragem. As Camadas de Filtragem não têm suporte "
"para usar os filtros de integração externa do G'Mic."

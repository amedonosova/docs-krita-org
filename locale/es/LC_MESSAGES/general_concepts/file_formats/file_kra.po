# Spanish translations for docs_krita_org_general_concepts___file_formats___file_kra.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___file_formats___file_kra\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-19 20:06+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.2\n"

#: ../../general_concepts/file_formats/file_kra.rst:1
msgid "The Krita Raster Archive file format."
msgstr ""

#: ../../general_concepts/file_formats/file_kra.rst:10
#, fuzzy
#| msgid "\\*.kra"
msgid "*.kra"
msgstr "\\*.kra"

#: ../../general_concepts/file_formats/file_kra.rst:10
msgid "KRA"
msgstr ""

#: ../../general_concepts/file_formats/file_kra.rst:10
msgid "Krita Archive"
msgstr ""

#: ../../general_concepts/file_formats/file_kra.rst:15
msgid "\\*.kra"
msgstr "\\*.kra"

#: ../../general_concepts/file_formats/file_kra.rst:17
msgid ""
".kra is Krita's internal file-format, which means that it is the file format "
"that saves all of the features Krita can handle. It's construction is "
"vaguely based on the open document standard, which means that you can rename "
"your .kra file to a .zip file and open it up to look at the insides."
msgstr ""

#: ../../general_concepts/file_formats/file_kra.rst:19
msgid ""
"It is a format that you can expect to get very heavy, and isn't meant for "
"sharing on the internet."
msgstr ""

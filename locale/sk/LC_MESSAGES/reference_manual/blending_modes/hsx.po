# translation of docs_krita_org_reference_manual___blending_modes___hsx.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___blending_modes___hsx\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-27 13:32+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/blending_modes/hsx.rst:1
msgid ""
"Page about the HSX blending modes in Krita, amongst which Hue, Color, "
"Luminosity and Saturation."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:11
#: ../../reference_manual/blending_modes/hsx.rst:202
msgid "Intensity"
msgstr "Intenzita"

#: ../../reference_manual/blending_modes/hsx.rst:11
#: ../../reference_manual/blending_modes/hsx.rst:214
msgid "Value"
msgstr "Hodnota"

#: ../../reference_manual/blending_modes/hsx.rst:11
#: ../../reference_manual/blending_modes/hsx.rst:226
msgid "Lightness"
msgstr "Jas"

#: ../../reference_manual/blending_modes/hsx.rst:11
#: ../../reference_manual/blending_modes/hsx.rst:238
msgid "Luminosity"
msgstr "Svietivosť"

#: ../../reference_manual/blending_modes/hsx.rst:11
msgid "Hue"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:11
msgid "Saturation"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:11
msgid "Luma"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:11
#, fuzzy
#| msgid "Lightness"
msgid "Brightness"
msgstr "Jas"

#: ../../reference_manual/blending_modes/hsx.rst:15
msgid "HSX"
msgstr "HSX"

#: ../../reference_manual/blending_modes/hsx.rst:17
msgid ""
"Krita has four different HSX coordinate systems. The difference between them "
"is how they handle tone."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:20
msgid "HSI"
msgstr "HSI"

#: ../../reference_manual/blending_modes/hsx.rst:22
msgid ""
"HSI is a color coordinate system, using Hue, Saturation and Intensity to "
"categorize a color. Hue is roughly the wavelength, whether the color is red, "
"yellow, green, cyan, blue or purple. It is measure in 360°, with 0 being "
"red. Saturation is the measurement of how close a color is to gray. "
"Intensity, in this case is the tone of the color. What makes intensity "
"special is that it recognizes yellow (rgb:1,1,0) having a higher combined "
"rgb value than blue (rgb:0,0,1). This is a non-linear tone dimension, which "
"means it's gamma-corrected."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:28
msgid "HSL"
msgstr "HSL"

#: ../../reference_manual/blending_modes/hsx.rst:30
msgid ""
"HSL is also a color coordinate system. It describes colors in Hue, "
"Saturation and Lightness. Lightness specifically puts both yellow "
"(rgb:1,1,0), blue (rgb:0,0,1) and middle gray (rgb:0.5,0.5,0.5) at the same "
"lightness (0.5)."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:34
msgid "HSV"
msgstr "HSV"

#: ../../reference_manual/blending_modes/hsx.rst:36
msgid ""
"HSV, occasionally called HSB, is a color coordinate system. It measures "
"colors in Hue, Saturation, and Value (also called Brightness). Value or "
"Brightness specifically refers to strength at which the pixel-lights on your "
"monitor have to shine. It sets Yellow (rgb:1,1,0), Blue (rgb:0,0,1) and "
"White (rgb:1,1,0) at the same Value (100%)"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:40
msgid "HSY"
msgstr "HSY"

#: ../../reference_manual/blending_modes/hsx.rst:42
msgid ""
"HSY is a color coordinate system. It categorizes colors in Hue, Saturation "
"and Luminosity. Well, not really, it uses Luma instead of true luminosity, "
"the difference being that Luminosity is linear while Luma is gamma-corrected "
"and just weights the rgb components. Luma is based on scientific studies of "
"how much light a color reflects in real-life. While like intensity it "
"acknowledges that yellow (rgb:1,1,0) is lighter than blue (rgb:0,0,1), it "
"also acknowledges that yellow (rgb:1,1,0) is lighter than cyan (rgb:0,1,1), "
"based on these studies."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:46
msgid "HSX Blending Modes"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:55
msgid "Color, HSV, HSI, HSL, HSY"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:57
msgid ""
"This takes the Luminosity/Value/Intensity/Lightness of the colors on the "
"lower layer, and combines them with the Saturation and Hue of the upper "
"pixels. We refer to Color HSY as 'Color' in line with other applications."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:62
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Color_HSI_Gray_0.4_and_Gray_0.5.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSI_Gray_0.4_and_Gray_0.5.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Color_HSI_Gray_0.4_and_Gray_0.5.png"

#: ../../reference_manual/blending_modes/hsx.rst:62
#: ../../reference_manual/blending_modes/hsx.rst:67
#: ../../reference_manual/blending_modes/hsx.rst:72
msgid "Left: **Normal**. Right: **Color HSI**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:67
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Color_HSI_Light_blue_and_Orange.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSI_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Color_HSI_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/hsx.rst:72
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Color_HSI_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSI_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Color_HSI_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:78
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Color_HSL_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSL_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Color_HSL_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:78
msgid "Left: **Normal**. Right: **Color HSL**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:84
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Color_HSV_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSV_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Color_HSV_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:84
msgid "Left: **Normal**. Right: **Color HSV**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:90
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Color_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/Blending_modes_Color_Sample_image_with_dots."
"png"

#: ../../reference_manual/blending_modes/hsx.rst:90
msgid "Left: **Normal**. Right: **Color**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:99
msgid "Hue HSV, HSI, HSL, HSY"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:101
msgid ""
"Takes the saturation and tone of the lower layer and combines them with the "
"hue of the upper-layer. Tone in this case being either Value, Lightness, "
"Intensity or Luminosity."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:107
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Hue_HSI_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Hue_HSI_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Hue_HSI_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:107
msgid "Left: **Normal**. Right: **Hue HSI**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:113
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Hue_HSL_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Hue_HSL_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Hue_HSL_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:113
msgid "Left: **Normal**. Right: **Hue HSL**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:119
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Hue_HSV_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Hue_HSV_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Hue_HSV_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:119
msgid "Left: **Normal**. Right: **Hue HSV**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:125
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Hue_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Hue_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/Blending_modes_Hue_Sample_image_with_dots."
"png"

#: ../../reference_manual/blending_modes/hsx.rst:125
msgid "Left: **Normal**. Right: **Hue**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:134
msgid "Increase Value, Lightness, Intensity or Luminosity."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:136
msgid ""
"Similar to lighten, but specific to tone. Checks whether the upper layer's "
"pixel has a higher tone than the lower layer's pixel. If so, the tone is "
"increased, if not, the lower layer's tone is maintained."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:142
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Increase_Intensity_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Intensity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Intensity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:142
msgid "Left: **Normal**. Right: **Increase Intensity**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:148
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Increase_Lightness_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Lightness_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Lightness_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:148
msgid "Left: **Normal**. Right: **Increase Lightness**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:154
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Increase_Value_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Value_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Value_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:154
msgid "Left: **Normal**. Right: **Increase Value**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:160
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Increase_Luminosity_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Luminosity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Luminosity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:160
msgid "Left: **Normal**. Right: **Increase Luminosity**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:170
msgid "Increase Saturation HSI, HSV, HSL, HSY"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:172
msgid ""
"Similar to lighten, but specific to Saturation. Checks whether the upper "
"layer's pixel has a higher Saturation than the lower layer's pixel. If so, "
"the Saturation is increased, if not, the lower layer's Saturation is "
"maintained."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:178
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Increase_Saturation_HSI_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Saturation_HSI_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Saturation_HSI_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:178
msgid "Left: **Normal**. Right: **Increase Saturation HSI**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:184
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Increase_Saturation_HSL_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Saturation_HSL_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Saturation_HSL_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:184
msgid "Left: **Normal**. Right: **Increase Saturation HSL**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:190
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Increase_Saturation_HSV_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Saturation_HSV_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Saturation_HSV_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:190
msgid "Left: **Normal**. Right: **Increase Saturation HSV**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:196
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Increase_Saturation_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Saturation_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Saturation_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:196
msgid "Left: **Normal**. Right: **Increase Saturation**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:204
msgid ""
"Takes the Hue and Saturation of the Lower layer and outputs them with the "
"intensity of the upper layer."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:209
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Intensity_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Intensity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Intensity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:209
msgid "Left: **Normal**. Right: **Intensity**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:216
msgid ""
"Takes the Hue and Saturation of the Lower layer and outputs them with the "
"Value of the upper layer."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:221
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Value_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Value_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/Blending_modes_Value_Sample_image_with_dots."
"png"

#: ../../reference_manual/blending_modes/hsx.rst:221
msgid "Left: **Normal**. Right: **Value**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:228
msgid ""
"Takes the Hue and Saturation of the Lower layer and outputs them with the "
"Lightness of the upper layer."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:233
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Lightness_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Lightness_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Lightness_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:233
msgid "Left: **Normal**. Right: **Lightness**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:240
msgid ""
"As explained above, actually Luma, but called this way as it's in line with "
"the terminology in other applications. Takes the Hue and Saturation of the "
"Lower layer and outputs them with the Luminosity of the upper layer. The "
"most preferred one of the four Tone blending modes, as this one gives fairly "
"intuitive results for the Tone of a hue"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:247
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Luminosity_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Luminosity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Luminosity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:247
msgid "Left: **Normal**. Right: **Luminosity**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:256
msgid "Saturation HSI, HSV, HSL, HSY"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:258
msgid ""
"Takes the Intensity and Hue of the lower layer, and outputs them with the "
"HSI saturation of the upper layer."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:263
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Saturation_HSI_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Saturation_HSI_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Saturation_HSI_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:263
msgid "Left: **Normal**. Right: **Saturation HSI**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:269
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Saturation_HSL_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Saturation_HSL_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Saturation_HSL_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:269
msgid "Left: **Normal**. Right: **Saturation HSL**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:275
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Saturation_HSV_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Saturation_HSV_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Saturation_HSV_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:275
msgid "Left: **Normal**. Right: **Saturation HSV**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:281
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Saturation_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Saturation_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Saturation_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:281
msgid "Left: **Normal**. Right: **Saturation**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:289
msgid "Decrease Value, Lightness, Intensity or Luminosity"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:291
msgid ""
"Similar to darken, but specific to tone. Checks whether the upper layer's "
"pixel has a lower tone than the lower layer's pixel. If so, the tone is "
"decreased, if not, the lower layer's tone is maintained."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:297
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Intensity_Gray_0.4_and_Gray_0.5.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Intensity_Gray_0.4_and_Gray_0.5.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Intensity_Gray_0.4_and_Gray_0.5.png"

#: ../../reference_manual/blending_modes/hsx.rst:297
#: ../../reference_manual/blending_modes/hsx.rst:302
#: ../../reference_manual/blending_modes/hsx.rst:307
msgid "Left: **Normal**. Right: **Decrease Intensity**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:302
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Intensity_Light_blue_and_Orange.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Intensity_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Intensity_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/hsx.rst:307
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Intensity_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Intensity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Intensity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:313
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Lightness_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Lightness_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Lightness_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:313
msgid "Left: **Normal**. Right: **Decrease Lightness**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:319
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Value_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Value_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Value_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:319
msgid "Left: **Normal**. Right: **Decrease Value**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:325
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Luminosity_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Luminosity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Luminosity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:325
msgid "Left: **Normal**. Right: **Decrease Luminosity**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:334
msgid "Decrease Saturation HSI, HSV, HSL, HSY"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:336
msgid ""
"Similar to darken, but specific to Saturation. Checks whether the upper "
"layer's pixel has a lower Saturation than the lower layer's pixel. If so, "
"the Saturation is decreased, if not, the lower layer's Saturation is "
"maintained."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:342
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Saturation_HSI_Gray_0.4_and_Gray_0.5.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSI_Gray_0.4_and_Gray_0.5.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_HSI_Gray_0.4_and_Gray_0.5.png"

#: ../../reference_manual/blending_modes/hsx.rst:342
#: ../../reference_manual/blending_modes/hsx.rst:347
#: ../../reference_manual/blending_modes/hsx.rst:352
msgid "Left: **Normal**. Right: **Decrease Saturation HSI**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:347
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Saturation_HSI_Light_blue_and_Orange.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSI_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_HSI_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/hsx.rst:352
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Saturation_HSI_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSI_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_HSI_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:358
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Saturation_HSL_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSL_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_HSL_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:358
msgid "Left: **Normal**. Right: **Decrease Saturation HSL**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:364
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Saturation_HSV_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSV_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_HSV_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:364
msgid "Left: **Normal**. Right: **Decrease Saturation HSV**."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:370
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Saturation_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:370
msgid "Left: **Normal**. Right: **Decrease Saturation**."
msgstr ""

# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:23+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/dockers/specific_color_selector.rst:1
msgid "Overview of the specific color selector docker."
msgstr "Översikt av den specifika färgväljarpanelen."

#: ../../reference_manual/dockers/specific_color_selector.rst:11
#: ../../reference_manual/dockers/specific_color_selector.rst:16
msgid "Specific Color Selector"
msgstr "Specifik färgväljare"

#: ../../reference_manual/dockers/specific_color_selector.rst:11
msgid "Color"
msgstr "Färg"

#: ../../reference_manual/dockers/specific_color_selector.rst:11
msgid "Color Selector"
msgstr "Färgväljare"

#: ../../reference_manual/dockers/specific_color_selector.rst:11
msgid "Color Space"
msgstr "Färgrymd"

#: ../../reference_manual/dockers/specific_color_selector.rst:19
msgid ".. image:: images/dockers/Krita_Specific_Color_Selector_Docker.png"
msgstr ".. image:: images/dockers/Krita_Specific_Color_Selector_Docker.png"

#: ../../reference_manual/dockers/specific_color_selector.rst:20
msgid ""
"The specific color selector allows you to choose specific colors within a "
"color space."
msgstr ""
"Den specifika färgväljaren låter dig välja specifika färger inom en färgrymd."

#: ../../reference_manual/dockers/specific_color_selector.rst:23
msgid "Color Space Chooser"
msgstr "Färgrymdsväljare"

#: ../../reference_manual/dockers/specific_color_selector.rst:25
msgid ""
"Fairly straightforward. This color space chooser allows you to pick the "
"color space, the bit depth and the icc profile in which you are going to "
"pick your color. Use the checkbox 'show color space selector' to hide this "
"feature."
msgstr ""
"Ganska rättfram. Denna färgrymdsväljare låter dig välja färgrymd, bitdjup "
"och ICC-profil som färgen ska hämtas från. Använd kryssrutan 'visa "
"färgrymdsväljare' för att dölja funktionen."

#: ../../reference_manual/dockers/specific_color_selector.rst:29
msgid "Sliders"
msgstr "Skjutreglage"

#: ../../reference_manual/dockers/specific_color_selector.rst:31
msgid ""
"These change per color space. If you chose 16bit float or 32 bit float, "
"these will go from 0 to 1.0, with the decimals deciding the difference "
"between colors."
msgstr ""
"De ändras per färgrymd. Om 16-bitars flyttal eller 32-bitars flyttal väljes, "
"går de från 0 till 1,0, och decimalerna bestämmer skillnaden mellan färger."

#: ../../reference_manual/dockers/specific_color_selector.rst:35
msgid "Hex Color Selector"
msgstr "Hexadecimal färgväljare"

#: ../../reference_manual/dockers/specific_color_selector.rst:37
msgid ""
"This is only available for the color spaces with a depth of 8 bit. This "
"allows you to input hex color codes, and receive the RGB, CMYK, LAB, XYZ or "
"YCrCb equivalent, and the other way around!"
msgstr ""
"Bara tillgänglig för färgrymder med djupet 8 bitar. Den låter dig mata in "
"hexadecimala färgkoder, och få deras ekvivalens i RGB, CMYK, LAB, XYZ eller "
"YCrCb, och motsatsen."

msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../<generated>:1
msgid "List of open documents."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:1
msgid "The window menu in Krita."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:11
msgid "Window"
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:11
msgid "View"
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:16
msgid "Window Menu"
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:18
msgid "A menu completely dedicated to window management in Krita."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:20
msgid "New Window"
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:21
msgid "Creates a new window for Krita. Useful with multiple screens."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:22
msgid "New View"
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:23
msgid ""
"Make a new view of the given document. You can have different zoom or "
"rotation on these."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:24
msgid "Workspace"
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:25
msgid "A convenient access panel to the :ref:`resource_workspaces`."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:26
msgid "Close"
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:27
msgid "Close the current view."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:28
msgid "Close All"
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:29
msgid "Close all documents"
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:30
msgid "Tile"
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:31
msgid "Tiles all open documents into a little sub-window."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:32
msgid "Cascade"
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:33
msgid "Cascades the sub-windows."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:34
msgid "Next"
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:35
msgid "Selects the next view."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:36
msgid "Previous"
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:37
msgid "Selects the previous view."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:39
msgid "Use this to switch between documents."
msgstr ""

msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-02 12:01\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_general_concepts___colors___color_models.pot\n"

#: ../../general_concepts/colors/color_models.rst:1
msgid "Color Models in Krita"
msgstr "Krita 的色彩模型"

#: ../../general_concepts/colors/color_models.rst:10
#: ../../general_concepts/colors/color_models.rst:15
msgid "Color Models"
msgstr "色彩模型"

#: ../../general_concepts/colors/color_models.rst:10
msgid "Color"
msgstr ""

#: ../../general_concepts/colors/color_models.rst:17
msgid ""
"Krita has many different color spaces and models. Following here is a brief "
"explanation of each, and their use-cases."
msgstr ""
"Krita 支持多种色彩空间和色彩模型。本文将对它们进行简要介绍并推荐各个模型的用"
"途。"

#: ../../general_concepts/colors/color_models.rst:22
msgid "RGB"
msgstr "RGB 模型"

#: ../../general_concepts/colors/color_models.rst:24
msgid "Red, Green, Blue."
msgstr "红 (Red)、绿 (Green)、蓝 (Blue)。"

#: ../../general_concepts/colors/color_models.rst:26
msgid ""
"These are the most efficient primaries for light-based color mixing, like "
"computer screens. Adding Red, Green and Blue light together results in "
"White, and is thus named the additive color wheel."
msgstr ""
"红绿蓝是光色混合时最有效的三原色，计算机的屏幕便是使用此模型。将红绿蓝混合后"
"得到白，因此这种模型也被叫做“加色法”模型。"

#: ../../general_concepts/colors/color_models.rst:28
msgid "RGB is used for two purposes:"
msgstr "RGB 模型有两类用途："

#: ../../general_concepts/colors/color_models.rst:30
msgid "Images that are meant for viewing on a screen:"
msgstr "第一类是用于在屏幕上观看的图像："

#: ../../general_concepts/colors/color_models.rst:32
msgid ""
"So that could be images for the web, buttons, avatars, or just portfolio "
"images."
msgstr "用于互联网的图像，如按钮、图标和头像等。"

#: ../../general_concepts/colors/color_models.rst:33
msgid "Or for Video games, both sprites and textures are best in RGB there."
msgstr "用于视频游戏的图像，如像素拼合图和材质等使用 RGB 模型的效果最好。"

#: ../../general_concepts/colors/color_models.rst:34
msgid "Or for 3d rendering, visual effects and cg animation."
msgstr "为 3D 渲染、视觉特效和 CG 动画准备的图像。"

#: ../../general_concepts/colors/color_models.rst:36
msgid ""
"And for the working space. A working space is an RGB gamut that is really "
"large and predictable, meaning it's good for image manipulation. You use "
"this next to a profiled monitor. This way you can have precise colors while "
"also being able to view them correctly on multiple screens."
msgstr ""
"第二类是用作工作空间。RGB 的色域更加宽广且容易预测，在该模型下建立的工作空间"
"对于图像处理更加有利。最好可以在一个带有色彩特性文件的显示器上使用此模型，这"
"样不但可以得到准确的颜色，还可以确保在其他屏幕上的显示效果保持一致。"

#: ../../general_concepts/colors/color_models.rst:39
msgid "Blending modes in RGB"
msgstr "RGB 模型中的混色模式"

#: ../../general_concepts/colors/color_models.rst:43
#: ../../general_concepts/colors/color_models.rst:85
#: ../../general_concepts/colors/color_models.rst:102
msgid "Color 1"
msgstr "颜色 1"

#: ../../general_concepts/colors/color_models.rst:43
#: ../../general_concepts/colors/color_models.rst:85
#: ../../general_concepts/colors/color_models.rst:102
msgid "Color 2"
msgstr "颜色 2"

#: ../../general_concepts/colors/color_models.rst:43
#: ../../general_concepts/colors/color_models.rst:85
#: ../../general_concepts/colors/color_models.rst:102
msgid "Normal"
msgstr "正常模式"

#: ../../general_concepts/colors/color_models.rst:43
#: ../../general_concepts/colors/color_models.rst:85
#: ../../general_concepts/colors/color_models.rst:102
msgid "Multiply"
msgstr "正片叠底"

#: ../../general_concepts/colors/color_models.rst:43
#: ../../general_concepts/colors/color_models.rst:85
#: ../../general_concepts/colors/color_models.rst:102
msgid "Screen"
msgstr "滤色模式"

#: ../../general_concepts/colors/color_models.rst:45
msgid "R"
msgstr "R"

#: ../../general_concepts/colors/color_models.rst:45
#: ../../general_concepts/colors/color_models.rst:87
msgid "G"
msgstr "G"

#: ../../general_concepts/colors/color_models.rst:45
msgid "B"
msgstr "B"

#: ../../general_concepts/colors/color_models.rst:47
#: ../../general_concepts/colors/color_models.rst:106
msgid "R & G"
msgstr "R & G"

#: ../../general_concepts/colors/color_models.rst:47
#: ../../general_concepts/colors/color_models.rst:106
msgid "1.0"
msgstr "1.0"

#: ../../general_concepts/colors/color_models.rst:47
#: ../../general_concepts/colors/color_models.rst:106
#: ../../general_concepts/colors/color_models.rst:108
#: ../../general_concepts/colors/color_models.rst:121
msgid "0.0"
msgstr "0.0"

#: ../../general_concepts/colors/color_models.rst:47
#: ../../general_concepts/colors/color_models.rst:49
#: ../../general_concepts/colors/color_models.rst:89
#: ../../general_concepts/colors/color_models.rst:106
#: ../../general_concepts/colors/color_models.rst:108
#: ../../general_concepts/colors/color_models.rst:119
#: ../../general_concepts/colors/color_models.rst:121
msgid "0.5"
msgstr "0.5"

#: ../../general_concepts/colors/color_models.rst:49
#: ../../general_concepts/colors/color_models.rst:89
#: ../../general_concepts/colors/color_models.rst:108
msgid "Gray"
msgstr "灰"

#: ../../general_concepts/colors/color_models.rst:49
#: ../../general_concepts/colors/color_models.rst:89
#: ../../general_concepts/colors/color_models.rst:106
#: ../../general_concepts/colors/color_models.rst:108
#: ../../general_concepts/colors/color_models.rst:119
#: ../../general_concepts/colors/color_models.rst:121
msgid "0.25"
msgstr "0.25"

#: ../../general_concepts/colors/color_models.rst:49
#: ../../general_concepts/colors/color_models.rst:89
#: ../../general_concepts/colors/color_models.rst:106
#: ../../general_concepts/colors/color_models.rst:108
#: ../../general_concepts/colors/color_models.rst:119
#: ../../general_concepts/colors/color_models.rst:121
msgid "0.75"
msgstr "0.75"

#: ../../general_concepts/colors/color_models.rst:55
msgid "RGB models: HSV, HSL, HSI and HSY"
msgstr "RGB 模型：HSV、HSL、HSI 和 HSY"

#: ../../general_concepts/colors/color_models.rst:57
msgid ""
"These are not included as their own color spaces in Krita. However, they do "
"show up in the blending modes and color selectors, so a brief overview:"
msgstr ""
"Krita 并未提供这些模型的独立色彩空间，但提供了对应每种模型的混色模式和拾色"
"器。下面对它们进行简要介绍："

#: ../../general_concepts/colors/color_models.rst:59
msgid "--Images of relationship rgb-hsv etc."
msgstr "(给 RGB-HSV 关系图等预留的位置)"

#: ../../general_concepts/colors/color_models.rst:61
msgid "Hue"
msgstr "色相 (Hue)"

#: ../../general_concepts/colors/color_models.rst:62
msgid ""
"The tint of a color, or, whether it's red, yellow, green, etc. Krita's Hue "
"is measured in 360 degrees, with 0 being red, 120 being green and 240 being "
"blue."
msgstr ""
"颜色的色调，例如红、黄 、绿等。Krita 的色相以 360 度来测量数值，0 度为红、"
"120 为绿、240 度为蓝。"

#: ../../general_concepts/colors/color_models.rst:63
msgid "Saturation"
msgstr "饱和度 (Saturation)"

#: ../../general_concepts/colors/color_models.rst:64
msgid ""
"How vibrant a color is. Saturation is slightly different between HSV and the "
"others. In HSV it's a measurement of the difference between two base colors "
"being used and three base colors being used. In the others it's a "
"measurement of how close a color is to gray, and sometimes this value is "
"called **Chroma**. Saturation ranges from 0 (gray) to 100 (pure color)."
msgstr ""
"颜色的鲜艳程度。饱和度在 HSV 模型中的作用与其他几个模型略有差别。在 HSV 模型"
"中饱和度是两种原色和三种原色数值差值的指标；在别的模型中是颜色与灰接近程度的"
"指标，有时也称作 **色品** 。饱和度的范围从 0 (灰) 到 100 (纯色) 。"

#: ../../general_concepts/colors/color_models.rst:65
msgid "Value"
msgstr "明度 (Value)"

#: ../../general_concepts/colors/color_models.rst:66
msgid ""
"Sometimes known as Brightness. Measurement of how much the pixel needs to "
"light up. Also measured from 0 to 100."
msgstr "有时也称作亮度，是像素被照亮程度的指标。范围也是从 0 到 100。"

#: ../../general_concepts/colors/color_models.rst:67
msgid "Lightness"
msgstr "亮度 (Lightness)"

#: ../../general_concepts/colors/color_models.rst:68
msgid ""
"Where a color aligns between white and black. This value is non-linear, and "
"puts all the most saturated possible colors at 50. Ranges from 0 to 100."
msgstr ""
"颜色在白与黑之间的相对位置。这是一个非线性数值，范围从 0 到 100，饱和度最高的"
"颜色位于 50。"

#: ../../general_concepts/colors/color_models.rst:69
msgid "Intensity"
msgstr "强度 (Intensity)"

#: ../../general_concepts/colors/color_models.rst:70
msgid ""
"Similar to lightness, except it acknowledges that yellow (1,1,0) is lighter "
"than blue (0,0,1). Ranges from 0 to 100."
msgstr ""
"与亮度 (Lightness) 类似，但将黄 (1,1,0) 视为比蓝 (0,0,1) 更亮。范围从 0 到 "
"100。"

#: ../../general_concepts/colors/color_models.rst:72
msgid "Luma (Y')"
msgstr "光度 (Luma, Y')"

#: ../../general_concepts/colors/color_models.rst:72
msgid ""
"Similar to lightness and Intensity, except it weights the red, green and "
"blue components based real-life measurements of how much light a color "
"reflects to determine its lightness. Ranges from 0 to 100. Luma is well "
"known for being used in film-color spaces."
msgstr ""
"与亮度 (Lightness) 和 强度 (Intensity) 类似，但通过实际测量颜色反射的光量来决"
"定亮度值。范围从 0 到 100。光度常见于电影行业的色彩空间。"

#: ../../general_concepts/colors/color_models.rst:77
msgid "Grayscale"
msgstr "灰阶模型"

#: ../../general_concepts/colors/color_models.rst:79
msgid ""
"This color space only registers gray values. This is useful, because by only "
"registering gray values, it only needs one channel of information, which in "
"turn means the image becomes much lighter in memory consumption!"
msgstr ""
"此色彩空间只记录灰度数值。它的有用之处在于只需要一条通道，因此图像消耗的内存"
"会少得多。"

#: ../../general_concepts/colors/color_models.rst:82
msgid ""
"This is useful for textures, but also anything else that needs to stay "
"grayscale, like Black and White comics."
msgstr ""
"灰度图常被用于制作纹理和贴图，也适用于任何需要限制颜色为灰度的图像，如黑白漫"
"画等。"

#: ../../general_concepts/colors/color_models.rst:95
msgid "CMYK"
msgstr "CMYK 模型"

#: ../../general_concepts/colors/color_models.rst:97
msgid "Cyan, Magenta, Yellow, Key"
msgstr "青 (Cyan)、品红 (Magenta)、黄 (Yellow)、黑 (Key)"

#: ../../general_concepts/colors/color_models.rst:99
msgid ""
"This is the color space of printers. Unlike computers, printers have these "
"four colors, and adding them all adds up to black instead of white. This is "
"thus also called a 'subtractive' color space."
msgstr ""
"这是用于打印机的色彩空间。和计算机屏幕的红绿蓝三原色混合后得到白不同，打印机"
"使用这四种颜色作为原色，它们混合后得到黑。因此这也被称作“减色法”模型。"

#: ../../general_concepts/colors/color_models.rst:104
#: ../../general_concepts/colors/color_models.rst:117
msgid "C"
msgstr "C"

#: ../../general_concepts/colors/color_models.rst:104
#: ../../general_concepts/colors/color_models.rst:117
msgid "M"
msgstr "M"

#: ../../general_concepts/colors/color_models.rst:104
#: ../../general_concepts/colors/color_models.rst:117
#: ../../general_concepts/colors/color_models.rst:165
msgid "Y"
msgstr "Y"

#: ../../general_concepts/colors/color_models.rst:104
#: ../../general_concepts/colors/color_models.rst:117
msgid "K"
msgstr "K"

#: ../../general_concepts/colors/color_models.rst:111
msgid ""
"There's also a difference between 'colored gray' and 'neutral gray' "
"depending on the profile."
msgstr "根据特性文件具体情况，灰色存在“彩色灰”和“中性灰”的差异。"

#: ../../general_concepts/colors/color_models.rst:115
msgid "25%"
msgstr "25%"

#: ../../general_concepts/colors/color_models.rst:115
msgid "50%"
msgstr "50%"

#: ../../general_concepts/colors/color_models.rst:115
msgid "75%"
msgstr "75%"

#: ../../general_concepts/colors/color_models.rst:119
msgid "Colored Gray"
msgstr "彩色灰"

#: ../../general_concepts/colors/color_models.rst:121
msgid "Neutral Gray"
msgstr "中性灰"

#: ../../general_concepts/colors/color_models.rst:128
msgid ".. image:: images/color_category/Cmyk_black_differences.png"
msgstr ""

#: ../../general_concepts/colors/color_models.rst:128
msgid ""
"In Krita, there's also the fact that the default color is a perfect black in "
"RGB, which then gets converted to our default CMYK in a funny manner, giving "
"a yellow look to the strokes. Again, another good reason to work in RGB and "
"let the conversion be done by the printing house."
msgstr ""
"Krita 的默认颜色是纯黑，它在 RGB 模型下面显示正常。可由于算法问题，它在 CMYK "
"模型下面画出来的笔画会偏黄。这也是为什么我们应该在 RGB 模型下面工作，把色彩转"
"换环节交给印刷公司来完成。"

#: ../../general_concepts/colors/color_models.rst:130
msgid ""
"While CMYK has a smaller 'gamut' than RGB, however, it's still recommended "
"to use an RGB working space profile to do your editing in. Afterwards, you "
"can convert it to your printer's CMYK profile using either perceptual or "
"relative colorimetric intent. Or you can just give the workspace rgb image "
"to your printer and let them handle the work."
msgstr ""
"尽管 CMYK 的色域比 RGB 的要小，但我们依然建议使用 RGB 工作空间特性文件来编辑"
"图像。你可以在编辑完成之后把成品图像通过可感知模式或者相对比色模式转换到打印"
"机的 CMYK 配置文件。你还可以直接把 RGB 图像输出到打印机，让它自行完成转换。"

#: ../../general_concepts/colors/color_models.rst:136
msgid "YCrCb"
msgstr "YCrCb 模型"

#: ../../general_concepts/colors/color_models.rst:138
msgid "Luminosity, Red-chroma, Blue-chroma"
msgstr "光度、红色品、蓝色品"

#: ../../general_concepts/colors/color_models.rst:140
msgid "YCrCb stands for"
msgstr "YCrCb 名称含义"

#: ../../general_concepts/colors/color_models.rst:142
msgid "Y'/Y"
msgstr "Y'/Y"

#: ../../general_concepts/colors/color_models.rst:143
msgid "Luma/Luminosity, thus, the amount of light a color reflects."
msgstr "光度 (Luma/Luminosity) 。颜色反射的光量。"

#: ../../general_concepts/colors/color_models.rst:144
msgid "Cr"
msgstr "Cr"

#: ../../general_concepts/colors/color_models.rst:145
msgid ""
"Red Chroma. This value measures how red a color is versus how green it is."
msgstr "红色品 (Red Chroma)。颜色所含红和绿的比例。"

#: ../../general_concepts/colors/color_models.rst:147
msgid "Cb"
msgstr "Cb"

#: ../../general_concepts/colors/color_models.rst:147
msgid ""
"Blue Chroma. This value measures how blue a color is versus how yellow it is."
msgstr "蓝色品 (Blue Chroma)。颜色所含蓝和黄的比例。"

#: ../../general_concepts/colors/color_models.rst:149
msgid ""
"This color space is often used in photography and in (correct) "
"implementations of JPEG. As a human you're much more sensitive to the "
"lightness of colors, and thus JPEG tries to compress the Cr and Cb channels, "
"and leave the Y channel in full quality."
msgstr ""
"此色彩空间常用于摄影，符合规范的 JPEG 算法也通过它来处理颜色。这是因为人类对"
"亮度的敏感程度要高于色彩，JPEG 利用了这一特性，只针对 Cr 和 Cb 两个颜色通道进"
"行有损压缩，但保持 Y 通道的品质不变。"

#: ../../general_concepts/colors/color_models.rst:153
msgid ""
"Krita doesn't bundle a ICC profile for YCrCb on the basis of there being no "
"open source ICC profiles for this color space. It's unusable without one, "
"and also probably very untested."
msgstr ""
"Krita 并未自带用于 YCrCb 的 ICC 特性文件，这是因为该色彩空间并无开源的 ICC 特"
"性文件可用。这造成了 Krita 严重缺乏相关测试，且在使用该空间时工作效果不稳定。"

#: ../../general_concepts/colors/color_models.rst:158
msgid "XYZ"
msgstr "XYZ 模型"

#: ../../general_concepts/colors/color_models.rst:160
msgid ""
"Back in 1931, the CIE (Institute of Color and Light), was studying human "
"color perception. In doing so, they made the first color spaces, with XYZ "
"being the one best at approximating human vision."
msgstr ""
"CIE 委员会 (国际照明委员会，Institute of Color and Light) 在 1931 年对人类的"
"颜色感知方式进行了研究，在此过程中他们发明了第一个色彩空间，其中 XYZ 空间最接"
"近人类视觉特点。"

#: ../../general_concepts/colors/color_models.rst:163
msgid "It's almost impossible to really explain what XYZ is."
msgstr "XYZ 空间的定义非常复杂，下面的介绍只是大概情况，并不精确。"

#: ../../general_concepts/colors/color_models.rst:166
msgid "is equal to green."
msgstr "等同于绿。"

#: ../../general_concepts/colors/color_models.rst:167
msgid "Z"
msgstr "Z"

#: ../../general_concepts/colors/color_models.rst:168
msgid "akin to blue."
msgstr "类似于蓝。"

#: ../../general_concepts/colors/color_models.rst:170
msgid "X"
msgstr "X"

#: ../../general_concepts/colors/color_models.rst:170
msgid "is supposed to be red."
msgstr "应该是红。"

#: ../../general_concepts/colors/color_models.rst:172
msgid ""
"XYZ is used as a baseline reference for all other profiles and models. All "
"color conversions are done in XYZ, and all profiles coordinates match XYZ."
msgstr ""
"XYZ 空间是所有其他色彩特性文件和色彩模型的参考基准。一切色彩转换都在 XYZ 空间"
"中进行，所有色彩空间的坐标都与 XYZ 空间匹配。"

#: ../../general_concepts/colors/color_models.rst:177
msgid "L\\*a\\*b\\*"
msgstr "L\\*a\\*b\\* 模型"

#: ../../general_concepts/colors/color_models.rst:179
msgid "Stands for:"
msgstr "名称含义："

#: ../../general_concepts/colors/color_models.rst:181
msgid "L\\*"
msgstr "L\\*"

#: ../../general_concepts/colors/color_models.rst:182
msgid "Lightness, similar to luminosity in this case."
msgstr "亮度，在此模型中与光度的特性类似。"

#: ../../general_concepts/colors/color_models.rst:183
msgid "a\\*"
msgstr "a\\*"

#: ../../general_concepts/colors/color_models.rst:184
msgid ""
"a\\* in this case is the measurement of how magenta a color is versus how "
"green it is."
msgstr "a\\* 是品红和绿的比例指标。"

#: ../../general_concepts/colors/color_models.rst:186
msgid "b\\*"
msgstr "b\\*"

#: ../../general_concepts/colors/color_models.rst:186
msgid ""
"b\\* in this case is a measurement of how yellow a color is versus how blue "
"a color is."
msgstr "b\\* 是黄和蓝的比例指标。"

#: ../../general_concepts/colors/color_models.rst:188
msgid ""
"L\\*a\\*b\\* is supposed to be a more comprehensible variety of XYZ and the "
"most 'complete' of all color spaces. It's often used as an in between color "
"space in conversion, but even more as the correct color space to do color-"
"balancing in. It's far easier to adjust the contrast and color tone in "
"L*a*b*."
msgstr ""
"L\\*a\\*b\\* 空间应该是 XYZ 空间的一个更易理解的变种，也是在所有色彩空间中色"
"域覆盖程度最为完整的一种。它常被用作色彩转换的过渡空间，也是色彩平衡作业应该"
"采用的色彩空间。在 L*a*b 空间中调整反差和色调远比其他方案要来的简单。"

#: ../../general_concepts/colors/color_models.rst:190
msgid ""
"L\\*a\\*b\\* is technically the same as Photoshop's LAB. Photoshop "
"specifically uses CIELAB d50."
msgstr ""
"L\\*a\\*b\\* 空间在技术层面与 Photoshop 的 LAB 空间一致。Photoshop 指定使用 "
"CIELAB d50。"

#: ../../general_concepts/colors/color_models.rst:193
msgid "Filters and blending modes"
msgstr "滤镜和混色模式"

#: ../../general_concepts/colors/color_models.rst:195
msgid ""
"Maybe you have noticed that blending modes in LAB don't work like they do in "
"RGB or CMYK. This is because the blending modes work by doing a bit of maths "
"on the color coordinates, and because color coordinates are different per "
"color space, the blending modes look different."
msgstr ""
"你可能会发现在 LAB 空间下面混色模式的效果与在 RGB 或者 CMYK 空间下面的不一"
"样。这是因为混色模式使用色彩坐标进行数学运算，而每个色彩空间的坐标系统都不一"
"样，因此混色结果也会有所不同。"

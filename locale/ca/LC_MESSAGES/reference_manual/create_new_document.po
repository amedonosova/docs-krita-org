# Translation of docs_krita_org_reference_manual___create_new_document.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-12 14:45+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/create_new_document.rst:1
msgid ""
"A simple guide to the first basic steps of using Krita: creating and saving "
"an image."
msgstr ""
"Una guia senzilla per als primers passos bàsics d'utilitzar el Krita: crear "
"i desar una imatge."

#: ../../reference_manual/create_new_document.rst:18
msgid "Save"
msgstr "Desar"

#: ../../reference_manual/create_new_document.rst:18
msgid "Load"
msgstr "Carregar"

#: ../../reference_manual/create_new_document.rst:18
msgid "New"
msgstr "Nova"

#: ../../reference_manual/create_new_document.rst:22
msgid "Create New Document"
msgstr "Crear un document nou"

#: ../../reference_manual/create_new_document.rst:24
msgid "A new document can be created as follows."
msgstr "Es pot crear un document nou de la següent manera."

#: ../../reference_manual/create_new_document.rst:26
msgid "Click on :guilabel:`File` from the application menu at the top."
msgstr ""
"Feu clic a :guilabel:`Fitxer` des del menú de l'aplicació a la part superior."

#: ../../reference_manual/create_new_document.rst:27
msgid ""
"Then click on :guilabel:`New`. Or you can do this by pressing :kbd:`Ctrl + "
"N`."
msgstr ""
"Després feu clic a :guilabel:`Nova`. O podeu fer-ho prement la drecera :kbd:"
"`Ctrl + N`."

#: ../../reference_manual/create_new_document.rst:28
msgid "Now you will get a New Document dialog box as shown below:"
msgstr "Ara obtindreu un diàleg de Document nou com es mostra a continuació:"

#: ../../reference_manual/create_new_document.rst:31
msgid ".. image:: images/Krita_newfile.png"
msgstr ".. image:: images/Krita_newfile.png"

#: ../../reference_manual/create_new_document.rst:32
msgid ""
"There are various sections in this dialog box which aid in creation of new "
"document, either using custom document properties or by using contents from "
"clipboard and templates. Following are the sections in this dialog box:"
msgstr ""
"Hi ha diverses seccions en aquest diàleg que ajudaran en la creació d'un "
"document nou, ja sigui utilitzant les propietats personalitzades del "
"document o utilitzant el contingut des del porta-retalls i les plantilles. "
"Les següents són les seccions en aquest diàleg:"

#: ../../reference_manual/create_new_document.rst:37
msgid "Custom Document"
msgstr "Document personalitzat"

#: ../../reference_manual/create_new_document.rst:39
msgid ""
"From this section you can create a document according to your requirements: "
"you can specify the dimensions, color model, bit depth, resolution, etc."
msgstr ""
"Des d'aquesta secció podreu crear un document segons els vostres requisits: "
"podeu especificar les dimensions, el model de color, la profunditat dels "
"bits, la resolució, etc."

# skip-rule: barb-uns
#: ../../reference_manual/create_new_document.rst:42
msgid ""
"In the top-most field of the :guilabel:`Dimensions` tab, from the Predefined "
"drop-down you can select predefined pixel sizes and PPI (pixels per inch). "
"You can also set custom dimensions and the orientation of the document from "
"the input fields below the predefined drop-down. This can also be saved as a "
"new predefined preset for your future use by giving a name in the Save As "
"field and clicking on the Save button. Below we find the Color section of "
"the new document dialog box, where you can select the color model and the "
"bit-depth. Check :ref:`general_concept_color` for more detailed information "
"regarding color."
msgstr ""
"En el camp superior de la pestanya :guilabel:`Dimensions`, des del "
"desplegable Predeterminat podreu seleccionar mides predefinides dels píxels "
"i PPP (píxels per polzada). També podreu establir unes dimensions "
"personalitzades i l'orientació del document des dels camps d'entrada a sota "
"del desplegable Predeterminat. També el podreu desar com a un ajustament "
"predefinit nou per al seu ús futur donant un nom en el camp Desa com a i "
"fent clic al botó Desa. A continuació, es troba la secció Color, la qual "
"permet seleccionar el model de color i la profunditat de bits. Comproveu "
"els :ref:`general_concept_color` per a una informació més detallada pel que "
"fa al color."

#: ../../reference_manual/create_new_document.rst:52
msgid ""
"On the :guilabel:`Content` tab, you can define a name for your new document. "
"This name will appear in the metadata of the file, and Krita will use it for "
"the auto-save functionality as well. If you leave it empty, the document "
"will be referred to as 'Unnamed' by default. You can select the background "
"color and the amount of layers you want in the new document. Krita remembers "
"the amount of layers you picked last time, so be careful."
msgstr ""
"A la pestanya :guilabel:`Contingut`, definireu un nom per al vostre "
"document. Aquest nom apareixerà en les metadades del fitxer, i el Krita "
"també el farà servir per a la funcionalitat de desament automàtic. Si el "
"deixeu buit, el document s'anomenarà «Sense nom» de manera predeterminada. "
"Podeu seleccionar el color de fons i la quantitat de capes que voleu en el "
"document nou. El Krita recordarà la quantitat de capes que vàreu triar "
"l'última vegada, així que aneu amb compte."

#: ../../reference_manual/create_new_document.rst:59
msgid ""
"Finally, there's a description box, useful to note down what you are going "
"to do."
msgstr "Finalment, hi ha un quadre de descripció útil per anotar el que fareu."

#: ../../reference_manual/create_new_document.rst:62
msgid "Create From Clipboard"
msgstr "Crear des del porta-retalls"

#: ../../reference_manual/create_new_document.rst:64
msgid ""
"This section allows you to create a document from an image that is in your "
"clipboard, like a screenshot. It will have all the fields set to match the "
"clipboard image."
msgstr ""
"Aquesta secció permet crear un document des d'una imatge que hi ha al porta-"
"retalls, com ara una captura de pantalla. Tindreu tots els camps establerts "
"per a coincidir amb la imatge del porta-retalls."

#: ../../reference_manual/create_new_document.rst:69
msgid "Templates:"
msgstr "Plantilles:"

#: ../../reference_manual/create_new_document.rst:71
msgid ""
"These are separate categories where we deliver special defaults. Templates "
"are just .kra files which are saved in a special location, so they can be "
"pulled up by Krita quickly. You can make your own template file from any ."
"kra file, by using :menuselection:`File --> Create Template From Image` in "
"the top menu. This will add your current document as a new template, "
"including all its properties along with the layers and layer contents."
msgstr ""
"Aquestes són les categories separades on hem lliurat valors predeterminats "
"especials. Les plantilles només són fitxers .kra que es desen en una "
"ubicació especial, de manera que el Krita els podrà recuperar amb rapidesa. "
"Podeu crear el vostre propi fitxer de plantilla des de qualsevol fitxer ."
"kra, emprant :menuselection:`Fitxer --> Crea una plantilla des de la imatge` "
"al menú principal. Això afegirà el vostre document actual com a una "
"plantilla personalitzada, incloses totes les seves propietats juntament amb "
"les capes i el contingut de la capa."

#: ../../reference_manual/create_new_document.rst:78
msgid ""
"Once you have created a new document according to your preference, you "
"should now have a white canvas in front of you (or whichever background "
"color you chose in the dialog)."
msgstr ""
"Un cop hàgiu creat un document nou segons la vostra preferència, ara hauríeu "
"de tenir un llenç blanc al davant (o el color del fons que heu triat al "
"diàleg)."
